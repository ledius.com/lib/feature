import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common';
import { FeatureService } from '@app/feature/services/feature.service';
import { Reflector } from '@nestjs/core';
import { FEATURES_KEY } from '@app/feature/decorators/features';
import { LoggerService } from '@ledius/logger';

@Injectable()
export class FeatureGuard implements CanActivate {
  constructor(
    private readonly featureService: FeatureService,
    private readonly loggerService: LoggerService,
    @Inject(Reflector.name) private readonly reflector: Reflector,
  ) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const contextHandler = context.getHandler();

    const features =
      this.reflector.get<string[]>(FEATURES_KEY, contextHandler) || [];

    const featurePass = features.every((feature) =>
      this.featureService.isOn(feature),
    );

    this.loggerService.log({
      message: `Feature checking`,
      use: features,
      pass: featurePass,
    });

    return featurePass;
  }
}
